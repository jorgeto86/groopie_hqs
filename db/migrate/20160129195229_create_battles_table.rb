class CreateBattlesTable < ActiveRecord::Migration
  def change
    create_table :battles do |t|
      t.datetime :date
      t.belongs_to :pet

      t.timestamps null: false
    end
 
    create_table :battles_pets, id: false do |t|
      t.belongs_to :battle, index: true
      t.belongs_to :pet, index: true
    end
  end
end
