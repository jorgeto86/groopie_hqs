class CreatePetsTable < ActiveRecord::Migration
  def change
    create_table :pets do |t|
      t.string :name
      t.belongs_to :admin_user, index: true
      t.integer :age
      t.string :gender
      t.string :type
      t.decimal :wins, default: 0.0
      t.decimal :defeats, default: 0.0
      t.decimal :ratio, default: 0.0

      t.timestamps
    end
  end
end
