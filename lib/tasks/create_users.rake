namespace :create_users do
  desc 'Creates first user'
  task create_first_user: :environment  do
    admin_user = AdminUser.create(name: 'test1', email: 'test1@test.com', password: '12345678')
    admin_user.skip_confirmation!
    admin_user.save
  end
end
