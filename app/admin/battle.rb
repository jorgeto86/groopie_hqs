ActiveAdmin.register Battle do
  permit_params :date, :winner, :pet_id, pet_ids: []

  index do
    selectable_column
    id_column
    column :date
    column :winner
    column :pets do |b|
      b.pets.pluck :name
    end
    actions
  end

  filter :name
  filter :winner
  filter :pets

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs "Battle Details" do
      f.input :date
      f.input :winner
      f.input :pets
    end
    f.actions
  end

end
