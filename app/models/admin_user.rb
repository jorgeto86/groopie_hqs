class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         :confirmable

  has_many :pets

  before_create :generate_authentication_token

  validates :email, presence: true

  private

  def generate_authentication_token
    loop do
      self.authentication_token = SecureRandom.base64(64)
      break unless AdminUser.find_by(authentication_token: authentication_token)
    end
  end
end
