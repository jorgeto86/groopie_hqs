class Battle < ActiveRecord::Base

  has_and_belongs_to_many :pets
  belongs_to :winner, foreign_key: "pet_id", class_name: "Pet"

  validate :pets_only_have_one_fight_per_day
  validate :two_pets_selected
  validate :two_pets_same_owner
  validates :winner, :date, presence: true

private

  def two_pets_selected
    if pet_ids.count == 2
      true
    else
      errors.add(:base, '2 pets must be selected')
      false
    end
  end

  def two_pets_same_owner
    if pets.present? && (pets.first.admin_user == pets.last.admin_user)
      errors.add(:base, 'Pets with same owner')
    end
  end

  def pets_only_have_one_fight_per_day
    if one_fight_per_day(pets.first) || one_fight_per_day(pets.last)
      errors.add(:base, 'Some pet already has got a fight this day')
      false
    else
      true
    end
  end

  def one_fight_per_day(pet)
    (pet && date) ? pet.battles.pluck(:date).map(&:to_date).include?(date.to_date) : false
  end
end
