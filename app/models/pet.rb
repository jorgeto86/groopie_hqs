class Pet < ActiveRecord::Base
  before_save :calculate_ratio

  belongs_to :admin_user
  has_and_belongs_to_many :battles

  validates :admin_user, presence: true

  def pet_type
    type.demodulize.downcase
  end

private

  def calculate_ratio
    self.ratio = wins/defeats unless defeats == 0
  end
end
