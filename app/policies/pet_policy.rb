class PetPolicy < ApplicationPolicy
  attr_reader :admin_user, :pet

  def initialize(admin_user, pet)
    @admin_user = admin_user
    @pet = pet
  end

  def update?
    admin_user == pet.admin_user
  end

  def destroy?
    admin_user == pet.admin_user
  end
end
