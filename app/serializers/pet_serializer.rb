class PetSerializer < ActiveModel::Serializer
  attributes(
    :age,
    :defeats,
    :admin_user_id,
    :gender,
    :id,
    :name,
    :ratio,
    :type,
    :wins
  )
end
