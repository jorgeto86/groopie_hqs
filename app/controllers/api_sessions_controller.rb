class ApiSessionsController < ApiController
  
  def create
    admin_user = AdminUser.find_by(email: create_params[:email])
    if admin_user && admin_user.valid_password?(create_params[:password])
      render json: {
        email: admin_user.email,
        token: admin_user.authentication_token,
        status: 201
      }
    else
      head :unauthorized 
    end
  end

  private

  def create_params
    params.require(:api_session).permit(:email, :password)
  end
end
