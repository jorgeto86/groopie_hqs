class PetsController < ApiController

  before_action :authenticate_user!
  before_action :pet, except: :index

  rescue_from ActiveRecord::RecordNotFound, with: :not_found
  rescue_from Pundit::NotAuthorizedError, with: :not_authorized

  def index
    @pets = Pet.all
    render json: @pets
  end

  def create
    @pet.update pet_params.merge(admin_user_id: @current_user.id)
    render json: @pet
  end

  def show
    render json: @pet
  end

  def update
    authorize @pet
    @pet.update pet_params
    render json: @pet
  end

  def destroy
    authorize @pet
    @pet.destroy
    render json: @pet
  end

  private

  def pet_params
    params.require(:pet).permit(
      :age,
      :defeats,
      :gender,
      :name,
      :ratio,
      :type,
      :wins
    )
  end

  def pet
    @pet ||= params[:id] ? Pet.find(params[:id]) : Pet.new
  end

  def authenticate_user!
    token, options =
      ActionController::HttpAuthentication::Token.token_and_options(request)

    user_email = options.blank?? nil : options[:email]
    user = user_email && AdminUser.find_by(email: user_email)

    if user && ActiveSupport::SecurityUtils.secure_compare(
        user.authentication_token, token)
      @current_user = user
    else
      head :unauthorized
    end
  end

  def current_user
    @current_user
  end

  def not_found
    render json: 'Not found', status: 404
  end

  def not_authorized
    render json: 'Not authorized', status: 401
  end
end
